//#![feature(core_intrinsics)]
#![feature(clone_from_slice)]
#![feature(zero_one)]

extern crate linalg;

extern crate byteorder;
extern crate libc;

use byteorder::{ReadBytesExt, WriteBytesExt, LittleEndian};

//use std::intrinsics::{type_name};
use std::io::{Read, Write};
use std::marker::{PhantomData};
use std::mem::{size_of};
use std::num::{Zero};
use std::slice::{from_raw_parts, from_raw_parts_mut};

pub mod linalg_blas;

pub trait ShapeDims: Copy {
  type Stride: Copy;

  fn len(&self) -> usize;
  fn offset(&self, stride: Self::Stride) -> usize;
  fn diff(&self, other: Self) -> Self;
}

impl ShapeDims for usize {
  type Stride = ();

  fn len(&self) -> usize {
    *self
  }

  fn offset(&self, _: ()) -> usize {
    *self
  }

  fn diff(&self, other: usize) -> usize {
    *self - other
  }
}

impl ShapeDims for (usize, usize) {
  type Stride = usize;

  fn len(&self) -> usize {
    self.0 * self.1
  }

  fn offset(&self, stride: usize) -> usize {
    self.0 + self.1 * stride
  }

  fn diff(&self, other: (usize, usize)) -> (usize, usize) {
    (self.0 - other.0, self.1 - other.1)
  }
}

impl ShapeDims for (usize, usize, usize) {
  type Stride = (usize, usize);

  fn len(&self) -> usize {
    self.0 * self.1 * self.2
  }

  fn offset(&self, stride: (usize, usize)) -> usize {
    self.0 + self.1 * stride.0 + self.2 * stride.1 * stride.0
  }

  fn diff(&self, other: (usize, usize, usize)) -> (usize, usize, usize) {
    (self.0 - other.0, self.1 - other.1, self.2 - other.2)
  }
}

#[derive(Clone, Copy, Eq, PartialEq, Debug)]
pub enum Shape {
  Shape1d(usize),
  Shape2d((usize, usize)),
  Shape3d((usize, usize, usize)),
}

impl From<Shape> for usize {
  fn from(shape: Shape) -> usize {
    match shape {
      Shape::Shape1d(dims) => dims,
      _ => panic!("shape to dims conversion mismatch!"),
    }
  }
}

impl From<Shape> for (usize, usize) {
  fn from(shape: Shape) -> (usize, usize) {
    match shape {
      Shape::Shape2d(dims) => dims,
      _ => panic!("shape to dims conversion mismatch!"),
    }
  }
}

impl From<Shape> for (usize, usize, usize) {
  fn from(shape: Shape) -> (usize, usize, usize) {
    match shape {
      Shape::Shape3d(dims) => dims,
      _ => panic!("shape to dims conversion mismatch!"),
    }
  }
}

impl From<usize> for Shape {
  fn from(dims: usize) -> Shape {
    Shape::Shape1d(dims)
  }
}

impl From<(usize, usize)> for Shape {
  fn from(dims: (usize, usize)) -> Shape {
    Shape::Shape2d(dims)
  }
}

impl From<(usize, usize, usize)> for Shape {
  fn from(dims: (usize, usize, usize)) -> Shape {
    Shape::Shape3d(dims)
  }
}

impl Shape {
  pub fn len(&self) -> usize {
    match self {
      &Shape::Shape1d(d) => d,
      &Shape::Shape2d((d0, d1)) => d0 * d1,
      &Shape::Shape3d((d0, d1, d2)) => d0 * d1 * d2,
    }
  }

  pub fn offset(&self, stride: &Shape) -> isize {
    match (*self, *stride) {
      (Shape::Shape2d((x0, x1)), Shape::Shape2d((y0, y1))) => {
        (x0 + x1 * y0) as isize
      }
      (Shape::Shape3d((x0, x1, x2)), Shape::Shape3d((y0, y1, y2))) => {
        (x0 + x1 * y0 + x2 * y1 * y0) as isize
      }
      _ => panic!("bad combination of shapes for offset!"),
    }
  }

  pub fn diff(&self, other: &Shape) -> Shape {
    match (*self, *other) {
      (Shape::Shape2d((x0, x1)), Shape::Shape2d((y0, y1))) => {
        Shape::Shape2d((x0 - y0, x1 - y1))
      }
      (Shape::Shape3d((x0, x1, x2)), Shape::Shape3d((y0, y1, y2))) => {
        Shape::Shape3d((x0 - y0, x1 - y1, x2 - y2))
      }
      _ => panic!("bad combination of shapes for diffing!"),
    }
  }

  pub fn join_last_dim(&self, other: &Shape) -> Shape {
    match (*self, *other) {
      (Shape::Shape2d((x0, x1)), Shape::Shape2d((y0, y1))) => {
        assert_eq!(x0, y0);
        Shape::Shape2d((x0, x1 + y1))
      }
      (Shape::Shape3d((x0, x1, x2)), Shape::Shape3d((y0, y1, y2))) => {
        assert_eq!(x0, y0);
        assert_eq!(x1, y1);
        Shape::Shape3d((x0, x1, x2 + y2))
      }
      _ => panic!("bad combination of shapes for joining!"),
    }
  }
}

pub trait View<'a, T, Dims>: Sized where T: 'a + Copy, Dims: Copy + Into<Shape> + From<Shape> {
  unsafe fn as_ptr(&'a self) -> *const T;
  fn get_bound(&self) -> Dims;
  fn get_stride(&self) -> Dims;

  fn len(&self) -> usize {
    let shape: Shape = Into::into(self.get_bound());
    shape.len()
  }

  fn size(&self) -> usize {
    size_of::<T>() * self.len()
  }
}

pub trait MutView<'a, T, Dims>: Sized where T: 'a + Copy, Dims: Copy + Into<Shape> + From<Shape> {
  //unsafe fn as_mut_ptr(&'a mut self) -> *mut T;
  unsafe fn as_mut_ptr(&mut self) -> *mut T;
  fn get_bound(&self) -> Dims;
  fn get_stride(&self) -> Dims;

  fn len(&self) -> usize {
    let shape: Shape = Into::into(self.get_bound());
    shape.len()
  }

  fn size(&self) -> usize {
    size_of::<T>() * self.len()
  }
}

pub trait WithZeros<T, Dims>: Sized where T: Zero, Dims: Copy + Into<Shape> + From<Shape> {
  fn with_zeros(dims: Dims) -> Self;
  fn zero_out(&mut self);
}

/*pub trait ArrayView<'a, T, Dims>: View<'a, T, Dims> {
  // TODO
}

pub trait ArrayMutView<'a, T, Dims>: MutView<'a, T, Dims> {
  // TODO
}*/

pub trait RegionView<'a, T, Dims>: View<'a, T, Dims> where T: 'a + Copy, Dims: Copy + Into<Shape> + From<Shape> {
  unsafe fn with_ptr(ptr: *const T, offset: isize, bound: Dims, stride: Dims) -> Self;

  fn view(&'a self, lower: Dims, upper: Dims) -> Self {
    let lower_shape: Shape = Into::into(lower);
    let upper_shape: Shape = Into::into(upper);
    let bound: Dims = From::from(upper_shape.diff(&lower_shape));
    let stride: Dims = self.get_stride();
    let offset = lower_shape.offset(&Into::into(stride));
    unsafe { Self::with_ptr(self.as_ptr(), offset, bound, stride) }
  }
}

pub trait RegionMutView<'a, T, Dims>: MutView<'a, T, Dims> where T: 'a + Copy, Dims: Copy + Into<Shape> + From<Shape> {
  type Immutable;

  unsafe fn with_mut_ptr(ptr: *mut T, offset: isize, bound: Dims, stride: Dims) -> Self;
  fn as_view(&'a self) -> Self::Immutable;

  fn mut_view(&'a mut self, lower: Dims, upper: Dims) -> Self {
    let lower_shape: Shape = Into::into(lower);
    let upper_shape: Shape = Into::into(upper);
    let bound: Dims = From::from(upper_shape.diff(&lower_shape));
    let stride: Dims = self.get_stride();
    let offset = lower_shape.offset(&Into::into(stride));
    unsafe { Self::with_mut_ptr(self.as_mut_ptr(), offset, bound, stride) }
  }
}

pub trait SerialDataTyExt: Copy {
  fn id() -> u8;
}

impl SerialDataTyExt for u8 {
  fn id() -> u8 { 0 }
}

impl SerialDataTyExt for f32 {
  fn id() -> u8 { 1 }
}

pub trait SerialFormat {}

pub struct NdArrayFormat;

impl SerialFormat for NdArrayFormat {}

pub trait ArraySerialize<'a, T, Dims, Format>: View<'a, T, Dims> where Format: SerialFormat, T: 'a + Copy, Dims: Copy + Into<Shape> + From<Shape> {
  fn serialize<W>(&'a self, writer: &mut W) where W: Write;
}

pub trait ArrayDeserialize<T, Format>: Sized where Format: SerialFormat, T: Copy {
  fn deserialize<R>(reader: &mut R) -> Result<Self, ()> where R: Read;

  // FIXME(20151014)
  //fn serial_size(dims: Dims) -> usize {
  fn serial_size(dims: (usize, usize, usize)) -> usize {
    unimplemented!();
  }
}

pub struct RegionBuf<T> where T: Copy {
  ptr: *mut T,
  length: usize,
  capacity: usize,
}

impl<T> RegionBuf<T> where T: Copy {
  pub unsafe fn new(ptr: *mut T, length: usize) -> RegionBuf<T> {
    RegionBuf{
      ptr: ptr,
      length: length,
      capacity: length,
    }
  }

  pub fn len(&self) -> usize {
    self.length
  }

  pub fn size(&self) -> usize {
    size_of::<T>() * self.len()
  }

  pub unsafe fn as_ptr(&self) -> *const T {
    self.ptr as *const T
  }

  pub unsafe fn as_mut_ptr(&self) -> *mut T {
    self.ptr
  }

  pub fn as_view2d<'a>(&'a self, bound: (usize, usize)) -> RegionView2d<'a, T> {
    assert!(self.length >= Shape::Shape2d(bound).len());
    RegionView2d{
      ptr: self.ptr as *const T,
      bound: bound,
      stride: bound,
      _marker: PhantomData,
    }
  }

  pub fn as_mut_view2d<'a>(&'a mut self, bound: (usize, usize)) -> RegionMutView2d<'a, T> {
    assert!(self.length >= Shape::Shape2d(bound).len());
    RegionMutView2d{
      ptr: self.ptr,
      bound: bound,
      stride: bound,
      _marker: PhantomData,
    }
  }

  pub fn as_view3d<'a>(&'a self, bound: (usize, usize, usize)) -> RegionView3d<'a, T> {
    assert!(self.length >= Shape::Shape3d(bound).len());
    RegionView3d{
      ptr: self.ptr,
      bound: bound,
      stride: bound,
      _marker: PhantomData,
    }
  }

  /*pub fn as_mut_view3d<'a>(&'a mut self, bound: (usize, usize, usize)) -> RegionMutView3d<'a, T> {
    assert!(self.length >= Shape::Shape3d(bound).len());
    RegionMutView3d{
      ptr: self.ptr,
      bound: bound,
      stride: bound,
      _marker: PhantomData,
    }
  }*/
}

pub struct RegionBufView<'a, T> where T: 'a + Copy {
  ptr: *const T,
  length: usize,
  capacity: usize,
  _marker: PhantomData<&'a T>,
}

impl<'a, T> RegionView<'a, T, usize> for RegionBufView<'a, T> where T: 'a + Copy {
  unsafe fn with_ptr(ptr: *const T, offset: isize, bound: usize, stride: usize) -> RegionBufView<'a, T> {
    RegionBufView{
      ptr: ptr.offset(offset),
      length: bound,
      capacity: stride,
      _marker: PhantomData,
    }
  }
}

impl<'a, T> View<'a, T, usize> for RegionBufView<'a, T> where T: 'a + Copy {
  unsafe fn as_ptr(&'a self) -> *const T {
    self.ptr
  }

  fn get_bound(&self) -> usize {
    self.length
  }

  fn get_stride(&self) -> usize {
    self.capacity
  }
}

pub struct RegionBufMutView<'a, T> where T: 'a + Copy {
  ptr: *mut T,
  length: usize,
  capacity: usize,
  _marker: PhantomData<&'a T>,
}

impl<'a, T> RegionMutView<'a, T, usize> for RegionBufMutView<'a, T> where T: 'a + Copy {
  type Immutable = RegionBufView<'a, T>;

  unsafe fn with_mut_ptr(ptr: *mut T, offset: isize, bound: usize, stride: usize) -> RegionBufMutView<'a, T> {
    RegionBufMutView{
      ptr: ptr.offset(offset),
      length: bound,
      capacity: stride,
      _marker: PhantomData,
    }
  }

  fn as_view(&'a self) -> RegionBufView<'a, T> {
    RegionBufView{
      ptr: self.ptr as *const T,
      length: self.length,
      capacity: self.capacity,
      _marker: PhantomData,
    }
  }
}

impl<'a, T> MutView<'a, T, usize> for RegionBufMutView<'a, T> where T: 'a + Copy {
  unsafe fn as_mut_ptr(&mut self) -> *mut T {
    self.ptr
  }

  fn get_bound(&self) -> usize {
    self.length
  }

  fn get_stride(&self) -> usize {
    self.capacity
  }
}

pub struct Region2d<T> where T: Copy {
  ptr: *mut T,
  bound: (usize, usize),
  stride: (usize, usize),
}

impl<T> Region2d<T> where T: Copy {
  pub unsafe fn new(ptr: *mut T, dims: (usize, usize)) -> Region2d<T> {
    Region2d{
      ptr: ptr,
      bound: dims,
      stride: dims,
    }
  }

  pub fn get_bound(&self) -> (usize, usize) {
    self.bound
  }

  pub fn get_stride(&self) -> (usize, usize) {
    self.stride
  }

  pub unsafe fn as_ptr(&self) -> *const T {
    self.ptr as *const T
  }

  pub unsafe fn as_mut_ptr(&self) -> *mut T {
    self.ptr
  }

  pub fn as_view<'a>(&'a self) -> RegionView2d<'a, T> {
    RegionView2d{
      ptr: self.ptr as *const T,
      bound: self.bound,
      stride: self.bound,
      _marker: PhantomData,
    }
  }

  pub fn as_mut_view<'a>(&'a mut self) -> RegionMutView2d<'a, T> {
    RegionMutView2d{
      ptr: self.ptr,
      bound: self.bound,
      stride: self.bound,
      _marker: PhantomData,
    }
  }
}

pub struct RegionView2d<'a, T> where T: 'a + Copy {
  ptr: *const T,
  bound: (usize, usize),
  stride: (usize, usize),
  _marker: PhantomData<&'a T>,
}

impl<'a, T> RegionView<'a, T, (usize, usize)> for RegionView2d<'a, T> where T: 'a + Copy {
  unsafe fn with_ptr(ptr: *const T, offset: isize, bound: (usize, usize), stride: (usize, usize)) -> RegionView2d<'a, T> {
    RegionView2d{
      ptr: ptr.offset(offset),
      bound: bound,
      stride: stride,
      _marker: PhantomData,
    }
  }
}

impl<'a, T> View<'a, T, (usize, usize)> for RegionView2d<'a, T> where T: 'a + Copy {
  unsafe fn as_ptr(&'a self) -> *const T {
    self.ptr
  }

  fn get_bound(&self) -> (usize, usize) {
    self.bound
  }

  fn get_stride(&self) -> (usize, usize) {
    self.stride
  }
}

pub struct RegionMutView2d<'a, T> where T: 'a + Copy {
  ptr: *mut T,
  bound: (usize, usize),
  stride: (usize, usize),
  _marker: PhantomData<&'a T>,
}

impl<'a, T> RegionMutView<'a, T, (usize, usize)> for RegionMutView2d<'a, T> where T: 'a + Copy {
  type Immutable = RegionView2d<'a, T>;

  unsafe fn with_mut_ptr(ptr: *mut T, offset: isize, bound: (usize, usize), stride: (usize, usize)) -> RegionMutView2d<'a, T> {
    RegionMutView2d{
      ptr: ptr.offset(offset),
      bound: bound,
      stride: stride,
      _marker: PhantomData,
    }
  }

  fn as_view(&'a self) -> RegionView2d<'a, T> {
    RegionView2d{
      ptr: self.ptr as *const T,
      bound: self.bound,
      stride: self.stride,
      _marker: PhantomData,
    }
  }
}

impl<'a, T> MutView<'a, T, (usize, usize)> for RegionMutView2d<'a, T> where T: 'a + Copy {
  unsafe fn as_mut_ptr(&mut self) -> *mut T {
    self.ptr
  }

  fn get_bound(&self) -> (usize, usize) {
    self.bound
  }

  fn get_stride(&self) -> (usize, usize) {
    self.stride
  }
}

pub struct Region3d<T> where T: Copy {
  ptr: *mut T,
  bound: (usize, usize, usize),
  stride: (usize, usize, usize),
}

impl<T> Region3d<T> where T: Copy {
  pub unsafe fn new(ptr: *mut T, dims: (usize, usize, usize)) -> Region3d<T> {
    Region3d{
      ptr: ptr,
      bound: dims,
      stride: dims,
    }
  }

  pub fn get_bound(&self) -> (usize, usize, usize) {
    self.bound
  }

  pub fn get_stride(&self) -> (usize, usize, usize) {
    self.stride
  }

  pub unsafe fn as_ptr(&self) -> *const T {
    self.ptr as *const T
  }

  pub unsafe fn as_mut_ptr(&self) -> *mut T {
    self.ptr
  }

  pub fn as_view<'a>(&'a self) -> RegionView3d<'a, T> {
    RegionView3d{
      ptr: self.ptr as *const T,
      bound: self.bound,
      stride: self.bound,
      _marker: PhantomData,
    }
  }

  pub fn as_mut_view<'a>(&'a mut self) -> RegionMutView3d<'a, T> {
    RegionMutView3d{
      ptr: self.ptr,
      bound: self.bound,
      stride: self.bound,
      _marker: PhantomData,
    }
  }
}

pub struct RegionView3d<'a, T> where T: 'a + Copy {
  ptr: *const T,
  bound: (usize, usize, usize),
  stride: (usize, usize, usize),
  _marker: PhantomData<&'a T>,
}

impl<'a, T> RegionView<'a, T, (usize, usize, usize)> for RegionView3d<'a, T> where T: 'a + Copy {
  unsafe fn with_ptr(ptr: *const T, offset: isize, bound: (usize, usize, usize), stride: (usize, usize, usize)) -> RegionView3d<'a, T> {
    RegionView3d{
      ptr: ptr.offset(offset),
      bound: bound,
      stride: stride,
      _marker: PhantomData,
    }
  }
}

impl<'a, T> View<'a, T, (usize, usize, usize)> for RegionView3d<'a, T> where T: 'a + Copy {
  unsafe fn as_ptr(&'a self) -> *const T {
    self.ptr
  }

  fn get_bound(&self) -> (usize, usize, usize) {
    self.bound
  }

  fn get_stride(&self) -> (usize, usize, usize) {
    self.stride
  }
}

pub struct RegionMutView3d<'a, T> where T: 'a + Copy {
  ptr: *mut T,
  bound: (usize, usize, usize),
  stride: (usize, usize, usize),
  _marker: PhantomData<&'a T>,
}

impl<'a, T> RegionMutView<'a, T, (usize, usize, usize)> for RegionMutView3d<'a, T> where T: 'a + Copy {
  type Immutable = RegionView3d<'a, T>;

  unsafe fn with_mut_ptr(ptr: *mut T, offset: isize, bound: (usize, usize, usize), stride: (usize, usize, usize)) -> RegionMutView3d<'a, T> {
    RegionMutView3d{
      ptr: ptr.offset(offset),
      bound: bound,
      stride: stride,
      _marker: PhantomData,
    }
  }

  fn as_view(&'a self) -> RegionView3d<'a, T> {
    RegionView3d{
      ptr: self.ptr as *const T,
      bound: self.bound,
      stride: self.stride,
      _marker: PhantomData,
    }
  }
}

impl<'a, T> MutView<'a, T, (usize, usize, usize)> for RegionMutView3d<'a, T> where T: 'a + Copy {
  unsafe fn as_mut_ptr(&mut self) -> *mut T {
    self.ptr
  }

  fn get_bound(&self) -> (usize, usize, usize) {
    self.bound
  }

  fn get_stride(&self) -> (usize, usize, usize) {
    self.stride
  }
}

pub struct Buffer<T> where T: Copy {
  data:     Vec<T>,
  length:   usize,
  capacity: usize,
}

impl<T> Buffer<T> where T: Copy {
  pub fn with_data(data: Vec<T>, length: usize, capacity: usize) -> Buffer<T> {
    assert_eq!(data.len(), capacity);
    assert!(length <= capacity);
    Buffer{
      data:     data,
      length:   length,
      capacity: capacity,
    }
  }

  pub fn as_view_3d<'a>(&'a self, dims: (usize, usize, usize)) -> ArrayView3d<'a, T> {
    let bound: Shape = From::from(dims);
    assert!(bound.len() <= self.length);
    ArrayView3d{
      data:   &self.data,
      bound:  dims,
      stride: dims,
    }
  }

  pub fn as_mut_view_3d<'a>(&'a mut self, dims: (usize, usize, usize)) -> ArrayMutView3d<'a, T> {
    let bound: Shape = From::from(dims);
    assert!(bound.len() <= self.length);
    ArrayMutView3d{
      data:   &mut self.data,
      bound:  dims,
      stride: dims,
    }
  }
}

#[derive(Clone, Debug)]
pub struct Array2d<T> where T: Copy {
  data: Vec<T>,
  bound: (usize, usize),
  stride: (usize, usize),
}

impl<'a> ArraySerialize<'a, f32, (usize, usize), NdArrayFormat> for ArrayView2d<'a, f32> {
  fn serialize<W>(&'a self, writer: &mut W) where W: Write {
    writer.write_u32::<LittleEndian>(0x0100444e)
      .ok().expect("failed to serialize!");
    writer.write_u32::<LittleEndian>(2)
      .ok().expect("failed to serialize!");
    let (bound0, bound1) = self.get_bound();
    writer.write_u64::<LittleEndian>(bound0 as u64)
      .ok().expect("failed to serialize!");
    writer.write_u64::<LittleEndian>(bound1 as u64)
      .ok().expect("failed to serialize!");
    if self.get_bound() == self.get_stride() {
      let bytes = unsafe { from_raw_parts(self.data.as_ptr() as *const u8, size_of::<f32>() * self.data.len()) };
      writer.write_all(bytes)
        .ok().expect("failed to serialize!");
    } else {
      // TODO
      unimplemented!();
    }
  }
}

impl ArrayDeserialize<f32, NdArrayFormat> for Array2d<f32> {
  fn deserialize<R>(reader: &mut R) -> Result<Array2d<f32>, ()> where R: Read {
    let magic0 = reader.read_u8()
      .ok().expect("failed to deserialize!");
    let magic1 = reader.read_u8()
      .ok().expect("failed to deserialize!");
    assert_eq!(magic0, b'N');
    assert_eq!(magic1, b'D');
    let version = reader.read_u8()
      .ok().expect("failed to deserialize!");
    assert_eq!(version, 0);
    let data_ty = reader.read_u8()
      .ok().expect("failed to deserialize!");
    let ndim = reader.read_u32::<LittleEndian>()
      .ok().expect("failed to deserialize!");
    assert_eq!(data_ty, 1);
    assert_eq!(ndim, 2);
    let bound0 = reader.read_u64::<LittleEndian>()
      .ok().expect("failed to deserialize!") as usize;
    let bound1 = reader.read_u64::<LittleEndian>()
      .ok().expect("failed to deserialize!") as usize;
    let dims = (bound0, bound1);
    let mut arr = unsafe { Array2d::new(dims) };
    {
      let mut data_bytes = unsafe { from_raw_parts_mut(arr.data.as_mut_ptr() as *mut u8, size_of::<f32>() * arr.data.len()) };
      let mut read_idx: usize = 0;
      loop {
        match reader.read(&mut data_bytes[read_idx ..]) {
          Ok(n) => {
            read_idx += n;
            if n == 0 {
              break;
            }
          }
          Err(e) => panic!("failed to deserialize: {}", e),
        }
      }
      assert_eq!(read_idx, data_bytes.len());
    }
    Ok(arr)
  }
}

impl<T> Array2d<T> where T: Copy + Zero {
  pub fn with_zeros(dims: (usize, usize)) -> Array2d<T> {
    let shape: Shape = From::from(dims);
    let mut data = Vec::with_capacity(shape.len());
    unsafe { data.set_len(shape.len()) };
    for i in (0 .. data.len()) {
      data[i] = Zero::zero();
    }
    Array2d{
      data: data,
      bound: dims,
      stride: dims,
    }
  }

  pub fn zero_out(&mut self) {
    for i in (0 .. self.data.len()) {
      self.data[i] = Zero::zero();
    }
  }

  pub fn as_slice(&self) -> &[T] {
    &self.data
  }

  pub fn as_mut_slice(&mut self) -> &mut [T] {
    &mut self.data
  }
}

impl<T> Array2d<T> where T: Copy {
  pub unsafe fn new(dims: (usize, usize)) -> Array2d<T> {
    let shape: Shape = From::from(dims);
    let mut data = Vec::with_capacity(shape.len());
    unsafe { data.set_len(shape.len()) };
    Array2d{
      data: data,
      bound: dims,
      stride: dims,
    }
  }

  pub fn with_data(data: Vec<T>, dims: (usize, usize)) -> Array2d<T> {
    let shape: Shape = From::from(dims);
    assert_eq!(data.len(), shape.len());
    Array2d{
      data: data,
      bound: dims,
      stride: dims,
    }
  }

  pub fn into_data(self) -> Vec<T> {
    self.data
  }

  pub fn as_view<'a>(&'a self) -> ArrayView2d<'a, T> {
    ArrayView2d{
      data: &self.data,
      bound: self.bound,
      stride: self.stride,
    }
  }

  pub fn as_mut_view<'a>(&'a mut self) -> ArrayMutView2d<'a, T> {
    ArrayMutView2d{
      data: &mut self.data,
      bound: self.bound,
      stride: self.stride,
    }
  }
}

pub struct ArrayView2d<'a, T> where T: 'a + Copy {
  data: &'a [T],
  bound: (usize, usize),
  stride: (usize, usize),
}

impl<'a, T> View<'a, T, (usize, usize)> for ArrayView2d<'a, T> where T: 'a + Copy {
  unsafe fn as_ptr(&'a self) -> *const T {
    self.data.as_ptr()
  }

  fn get_bound(&self) -> (usize, usize) {
    self.bound
  }

  fn get_stride(&self) -> (usize, usize) {
    self.stride
  }
}

impl<'a, T> ArrayView2d<'a, T> where T: 'a + Copy {
  pub fn as_slice(&self) -> &[T] {
    self.data
  }

  //pub fn view(&'a self, new_bound: (usize, usize)) -> ArrayView2d<'a, T> {
  pub fn view(self, lower: (usize, usize), upper: (usize, usize)) -> ArrayView2d<'a, T> {
    /*let (new_bound0, new_bound1) = new_bound;
    let (bound0, bound1) = self.bound;
    assert!(new_bound0 <= bound0);
    assert!(new_bound1 <= bound1);*/
    let lower_shape: Shape = Into::into(lower);
    let upper_shape: Shape = Into::into(upper);
    let bound: (usize, usize) = From::from(upper_shape.diff(&lower_shape));
    let stride: (usize, usize) = self.get_stride();
    let offset = lower_shape.offset(&Into::into(stride));
    let limit = upper_shape.offset(&Into::into(stride));
    ArrayView2d{
      data: &self.data[offset as usize .. limit as usize],
      bound: bound,
      stride: stride,
    }
  }

  pub fn send(&'a self, other: &'a mut ArrayMutView2d<'a, T>) {
    if self.bound == other.bound &&
        self.stride.0 == other.stride.0
    {
      other.data.clone_from_slice(self.data);
    } else {
      panic!("UNIMPLEMENTED: ArrayView2d.send() for strided case!");
    }
  }
}

pub struct ArrayMutView2d<'a, T> where T: 'a + Copy {
  data: &'a mut [T],
  bound: (usize, usize),
  stride: (usize, usize),
}

impl<'a, T> MutView<'a, T, (usize, usize)> for ArrayMutView2d<'a, T> where T: 'a + Copy {
  unsafe fn as_mut_ptr(&mut self) -> *mut T {
    self.data.as_mut_ptr()
  }

  fn get_bound(&self) -> (usize, usize) {
    self.bound
  }

  fn get_stride(&self) -> (usize, usize) {
    self.stride
  }
}

impl<'a, T> ArrayMutView2d<'a, T> where T: 'a + Copy {
  pub fn as_mut_slice(&'a mut self) -> &'a mut [T] {
    self.data
  }

  //pub fn mut_view(&'a mut self, new_bound: (usize, usize)) -> ArrayMutView2d<'a, T> {
  pub fn mut_view(mut self, lower: (usize, usize), upper: (usize, usize)) -> ArrayMutView2d<'a, T> {
    /*let (new_bound0, new_bound1) = new_bound;
    let (bound0, bound1) = self.bound;
    assert!(new_bound0 <= bound0);
    assert!(new_bound1 <= bound1);*/
    let lower_shape: Shape = Into::into(lower);
    let upper_shape: Shape = Into::into(upper);
    let bound: (usize, usize) = From::from(upper_shape.diff(&lower_shape));
    let stride: (usize, usize) = self.get_stride();
    let offset = lower_shape.offset(&Into::into(stride));
    let limit = upper_shape.offset(&Into::into(stride));
    ArrayMutView2d{
      data: &mut self.data[offset as usize .. limit as usize],
      bound: bound,
      stride: stride,
    }
  }
}

#[derive(Clone, Debug)]
pub struct Array3d<T> where T: Copy {
  data: Vec<T>,
  bound: (usize, usize, usize),
  stride: (usize, usize, usize),
}

impl<T> Array3d<T> where T: Copy + Zero {
  pub fn with_zeros(dims: (usize, usize, usize)) -> Array3d<T> {
    let shape: Shape = From::from(dims);
    let mut data = Vec::with_capacity(shape.len());
    unsafe { data.set_len(shape.len()) };
    //println!("DEBUG: Array3d::with_zeros(): {}", shape.len());
    for i in (0 .. data.len()) {
      data[i] = Zero::zero();
    }
    Array3d{
      data: data,
      bound: dims,
      stride: dims,
    }
  }

  pub fn zero_out(&mut self) {
    for i in (0 .. self.data.len()) {
      self.data[i] = Zero::zero();
    }
  }
}

impl<T> Array3d<T> where T: Copy {
  pub unsafe fn new(dims: (usize, usize, usize)) -> Array3d<T> {
    let shape: Shape = From::from(dims);
    let mut data = Vec::with_capacity(shape.len());
    unsafe { data.set_len(shape.len()) };
    Array3d{
      data: data,
      bound: dims,
      stride: dims,
    }
  }

  pub fn with_data(data: Vec<T>, dims: (usize, usize, usize)) -> Array3d<T> {
    let shape: Shape = From::from(dims);
    assert_eq!(data.len(), shape.len());
    Array3d{
      data: data,
      bound: dims,
      stride: dims,
    }
  }

  pub fn as_slice(&self) -> &[T] {
    &self.data
  }

  pub fn as_mut_slice(&mut self) -> &mut [T] {
    &mut self.data
  }

  pub fn as_view<'a>(&'a self) -> ArrayView3d<'a, T> {
    ArrayView3d{
      data: &self.data,
      bound: self.bound,
      stride: self.stride,
    }
  }

  pub fn as_mut_view<'a>(&'a mut self) -> ArrayMutView3d<'a, T> {
    ArrayMutView3d{
      data: &mut self.data,
      bound: self.bound,
      stride: self.stride,
    }
  }
}

pub struct ArrayView3d<'a, T> where T: 'a + Copy {
  data: &'a [T],
  bound: (usize, usize, usize),
  stride: (usize, usize, usize),
}

impl<'a, T> ArrayView3d<'a, T> where T: 'a + Copy {
  pub fn as_slice(&self) -> &[T] {
    self.data
  }
}

impl<'a, T> View<'a, T, (usize, usize, usize)> for ArrayView3d<'a, T> where T: 'a + Copy {
  unsafe fn as_ptr(&'a self) -> *const T {
    self.data.as_ptr()
  }

  fn get_bound(&self) -> (usize, usize, usize) {
    self.bound
  }

  fn get_stride(&self) -> (usize, usize, usize) {
    self.stride
  }
}

impl<'a, T> ArraySerialize<'a, T, (usize, usize, usize), NdArrayFormat> for ArrayView3d<'a, T> where T: 'a + SerialDataTyExt {
  fn serialize<W>(&'a self, writer: &mut W) where W: Write {
    let ty_id = T::id();
    writer.write_u32::<LittleEndian>(0x0000444e | ((ty_id as u32) << 24))
      .ok().expect("failed to serialize!");
    writer.write_u32::<LittleEndian>(3)
      .ok().expect("failed to serialize!");
    let (bound0, bound1, bound2) = self.get_bound();
    writer.write_u64::<LittleEndian>(bound0 as u64)
      .ok().expect("failed to serialize!");
    writer.write_u64::<LittleEndian>(bound1 as u64)
      .ok().expect("failed to serialize!");
    writer.write_u64::<LittleEndian>(bound2 as u64)
      .ok().expect("failed to serialize!");
    if self.get_bound() == self.get_stride() {
      let bytes = unsafe { from_raw_parts(self.data.as_ptr() as *const u8, size_of::<T>() * self.data.len()) };
      writer.write_all(bytes)
        .ok().expect("failed to serialize!");
    } else {
      // TODO
      unimplemented!();
    }
  }
}

impl<T> ArrayDeserialize<T, NdArrayFormat> for Array3d<T> where T: SerialDataTyExt {
  fn serial_size(dims: (usize, usize, usize)) -> usize {
    let shape = Shape::from(dims);
    32 + shape.len()
  }

  fn deserialize<R>(reader: &mut R) -> Result<Array3d<T>, ()> where R: Read {
    let magic0 = reader.read_u8()
      .ok().expect("failed to deserialize!");
    let magic1 = reader.read_u8()
      .ok().expect("failed to deserialize!");
    assert_eq!(magic0, b'N');
    assert_eq!(magic1, b'D');
    let version = reader.read_u8()
      .ok().expect("failed to deserialize!");
    assert_eq!(version, 0);
    let data_ty = reader.read_u8()
      .ok().expect("failed to deserialize!");
    let ndim = reader.read_u32::<LittleEndian>()
      .ok().expect("failed to deserialize!");
    let expected_data_ty = T::id();
    assert_eq!(data_ty, expected_data_ty);
    assert_eq!(ndim, 3);
    let bound0 = reader.read_u64::<LittleEndian>()
      .ok().expect("failed to deserialize!") as usize;
    let bound1 = reader.read_u64::<LittleEndian>()
      .ok().expect("failed to deserialize!") as usize;
    let bound2 = reader.read_u64::<LittleEndian>()
      .ok().expect("failed to deserialize!") as usize;
    let dims = (bound0, bound1, bound2);
    let mut arr = unsafe { Array3d::new(dims) };
    {
      let mut data_bytes = unsafe { from_raw_parts_mut(arr.data.as_mut_ptr() as *mut u8, size_of::<T>() * arr.data.len()) };
      let mut read_idx: usize = 0;
      loop {
        match reader.read(&mut data_bytes[read_idx ..]) {
          Ok(n) => {
            read_idx += n;
            if n == 0 {
              break;
            }
          }
          Err(e) => panic!("failed to deserialize: {}", e),
        }
      }
      assert_eq!(read_idx, data_bytes.len());
    }
    Ok(arr)
  }
}

pub struct ArrayMutView3d<'a, T> where T: 'a + Copy {
  data: &'a mut [T],
  bound: (usize, usize, usize),
  stride: (usize, usize, usize),
}

impl<'a, T> MutView<'a, T, (usize, usize, usize)> for ArrayMutView3d<'a, T> where T: 'a + Copy {
  unsafe fn as_mut_ptr(&mut self) -> *mut T {
    self.data.as_mut_ptr()
  }

  fn get_bound(&self) -> (usize, usize, usize) {
    self.bound
  }

  fn get_stride(&self) -> (usize, usize, usize) {
    self.stride
  }
}

impl<'a, T> ArrayMutView3d<'a, T> where T: 'a + Copy {
}
