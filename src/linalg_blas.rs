use super::{View, MutView, ArrayView2d, ArrayMutView2d};

use linalg::blas::{Blas, BVector, BMatrix, Transpose};
use libc::{c_char, c_int};

#[derive(Clone, Copy)]
#[repr(C)]
pub enum CblasOrder {
  RowMajor  = 101,
  ColMajor  = 102,
}

#[derive(Clone, Copy)]
#[repr(C)]
pub enum CblasTranspose {
  NoTrans     = 111,
  Trans       = 112,
  ConjTrans   = 113,
  ConjNoTrans = 114,
}

#[derive(Clone, Copy)]
#[repr(C)]
pub enum CblasUpLo {
  Upper = 121,
  Lower = 122,
}

#[derive(Clone, Copy)]
#[repr(C)]
pub enum CblasDiag {
  NonUnit = 131,
  Unit    = 132,
}

#[derive(Clone, Copy)]
#[repr(C)]
pub enum CblasSide {
  Left  = 141,
  Right = 142,
}

#[link(name = "openblas_haswellp-r0.2.14", kind = "dylib")]
extern "C" {
  pub fn openblas_set_num_threads(num_threads: c_int);
  pub fn openblas_get_num_threads() -> c_int;
  pub fn openblas_get_num_procs() -> c_int;
  pub fn openblas_get_config() -> *mut c_char;
  pub fn openblas_get_corename() -> *mut c_char;
  pub fn openblas_get_parallel() -> c_int;

  pub fn cblas_saxpy(
      n: c_int,
      alpha: f32,
      x: *const f32, incx: c_int,
      y: *mut f32, incy: c_int,
  );

  pub fn cblas_sgemv(
      order: CblasOrder, trans: CblasTranspose,
      m: c_int, n: c_int,
      alpha: f32,
      a: *const f32, lda: c_int,
      x: *const f32, incx: c_int,
      beta: f32,
      y: *mut f32, incy: c_int,
  );

  pub fn cblas_sgemm(
      order: CblasOrder, trans_a: CblasTranspose, trans_b: CblasTranspose,
      m: c_int, n: c_int, k: c_int,
      alpha: f32,
      a: *const f32, lda: c_int,
      b: *const f32, ldb: c_int,
      beta: f32,
      c: *mut f32, ldc: c_int,
  );
}

pub trait ToCblas<T> {
  type Output;

  fn to_cblas(&self) -> Self::Output;
}

impl ToCblas<Transpose> for Transpose {
  type Output = CblasTranspose;

  fn to_cblas(&self) -> CblasTranspose {
    match self {
      &Transpose::N => CblasTranspose::NoTrans,
      &Transpose::T => CblasTranspose::Trans,
      &Transpose::H => CblasTranspose::ConjTrans,
    }
  }
}

impl<'a> Blas<'a, f32> for ArrayMutView2d<'a, f32> {
  type Ctx = ();
}

impl<'a> BVector<'a, f32> for ArrayMutView2d<'a, f32> {
  type Matrix = ArrayView2d<'a, f32>;
  type Vector = ArrayView2d<'a, f32>;

  fn row_vector_sum(&mut self, alpha: f32, x: &ArrayView2d<'a, f32>, _: &()) {
    let (m, n) = self.get_bound();
    let (x_m, x_n) = x.get_bound();
    assert_eq!(n, x_n);
    assert_eq!(m, 1);
    assert_eq!(x_m, 1);
    let (incy, _) = self.get_stride();
    let (incx, _) = x.get_stride();
    unsafe { cblas_saxpy(
        n as i32,
        alpha,
        x.as_ptr(), incx as i32,
        self.as_mut_ptr(), incy as i32,
    ) };
  }
}

impl<'a> BMatrix<'a, f32> for ArrayMutView2d<'a, f32> {
  type Matrix = ArrayView2d<'a, f32>;
  type Vector = ArrayView2d<'a, f32>;

  /*fn matrix_sum(&mut self, alpha: f32, x: &ArrayView2d<'a, f32>, _: &()) {
    let (m, n) = self.get_bound();
    let (x_m, x_n) = x.get_bound();
    assert_eq!(n, x_n);
    assert_eq!(m, x_m);
    if self.get_bound() ==  self.get_stride() && x.get_bound() == x.get_stride() {
      // TODO
    }
  }*/

  fn matrix_prod(
      &mut self,
      alpha: f32,
      a: &ArrayView2d<f32>, trans_a: Transpose,
      b: &ArrayView2d<f32>, trans_b: Transpose,
      beta: f32,
      _: &())
  {
    let (m, n) = self.get_bound();
    let (a_m, a_n) = a.get_bound();
    let (b_m, b_n) = b.get_bound();
    let (at_m, at_n) = match trans_a {
      Transpose::N => (a_m, a_n),
      Transpose::T | Transpose::H => (a_n, a_m),
    };
    let (bt_m, bt_n) = match trans_b {
      Transpose::N => (b_m, b_n),
      Transpose::T | Transpose::H => (b_n, b_m),
    };
    assert_eq!(m, at_m);
    assert_eq!(n, bt_n);
    assert_eq!(at_n, bt_m);
    let k = at_n;
    let (lda, _) = a.get_stride();
    let (ldb, _) = b.get_stride();
    let (ldc, _) = self.get_stride();
    unsafe { cblas_sgemm(
        CblasOrder::ColMajor,
        trans_a.to_cblas(), trans_b.to_cblas(),
        m as i32, n as i32, k as i32,
        alpha,
        a.as_ptr(), lda as i32,
        b.as_ptr(), ldb as i32,
        beta,
        self.as_mut_ptr(), ldc as i32,
    ) };
  }
}

// TODO(20151020)
